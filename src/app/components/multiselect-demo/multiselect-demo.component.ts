import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, AbstractControl } from "@angular/forms";

@Component({
  selector: "app-multiselect-demo",
  templateUrl: "./multiselect-demo.component.html",
  styleUrls: ["./multiselect-demo.component.scss"]
})
export class MultiselectDemoComponent implements OnInit {
  dropdownHeight = 200;

  templateOptions = [
    {
      label: "English",
      value: "en",
      id: Math.round(Math.random() * 1000000)
    },
    {
      label: "French",
      value: "fr",
      id: Math.round(Math.random() * 1000000)
    },
    {
      label: "Spanish",
      value: "es",
      id: Math.round(Math.random() * 1000000)
    }
  ];

  reactiveOptions = [
    {
      label: "English",
      value: "en",
      id: Math.round(Math.random() * 1000000)
    },
    {
      label: "French",
      value: "fr",
      id: Math.round(Math.random() * 1000000)
    },
    {
      label: "Spanish",
      value: "es",
      id: Math.round(Math.random() * 1000000)
    }
  ];

  multiSelectValue = [
    {
      ...this.templateOptions[0],
      selected: true
    }
  ];

  templateHtml = `
    <form>
      <div [style.width.px]="300">
        <rbc-multiselect
          [options]="templateOptions"
          name="mutliselect"
          [height]="dropdownHeight"
          label="Multiselect Demo"
          [(ngModel)]="multiSelectValue"
        ></rbc-multiselect>
      </div>
    </form>
  `;

  reactiveHtml = `
    <form [formGroup]="form">
      <div [style.width.px]="300">
        <rbc-multiselect
          [options]="reactiveOptions"
          formControlName="mutliselect"
          [height]="dropdownHeight"
          label="Multiselect Demo"
        ></rbc-multiselect>
      </div>
    </form>
  `;

  form: FormGroup;
  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      multiselect: []
    });
  }

  get multiselect(): AbstractControl {
    return this.form.get("multiselect");
  }

  ngOnInit() {}
}
