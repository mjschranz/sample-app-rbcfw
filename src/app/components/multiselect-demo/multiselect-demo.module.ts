import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MultiselectDemoComponent } from "./multiselect-demo.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { WmMultiselectModule } from "rbc-wm-framework-angular";

@NgModule({
  declarations: [MultiselectDemoComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, WmMultiselectModule],
  exports: [MultiselectDemoComponent]
})
export class MultiselectDemoModule {}
