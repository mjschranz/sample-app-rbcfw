import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CardDemoComponent } from "./card-demo.component";
import { WmCardModule } from "rbc-wm-framework-angular";

@NgModule({
  declarations: [CardDemoComponent],
  imports: [CommonModule, WmCardModule],
  exports: [CardDemoComponent]
})
export class CardDemoModule {}
