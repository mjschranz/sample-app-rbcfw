import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { PointDemoComponent } from "./point-demo.component";

describe("PointDemoComponent", () => {
  let component: PointDemoComponent;
  let fixture: ComponentFixture<PointDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PointDemoComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
