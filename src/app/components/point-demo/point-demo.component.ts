import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-point-demo",
  templateUrl: "./point-demo.component.html",
  styleUrls: ["./point-demo.component.scss"]
})
export class PointDemoComponent implements OnInit {
  pointContent = `
    <div class="mt-1 d-flex jc-space-around">
      <rbc-point label="Step 2" number="3" helpText="Living expenses"></rbc-point>

      <rbc-point :checked="true" label="Step 2" number="22" helpText="Living expenses"></rbc-point>

      <rbc-point passed="true" label="Step 2" helpText="Living expenses"></rbc-point>

      <rbc-point completed="true" label="Step 2" helpText="Living expenses"></rbc-point>

      <rbc-point disabled="true" label="Step 2" helpText="Living expenses"></rbc-point>
    </div>
  `;
  constructor() {}

  ngOnInit() {}
}
