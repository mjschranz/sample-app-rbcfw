import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PointDemoComponent } from "./point-demo.component";
import { WmPointModule } from "rbc-wm-framework-angular";

@NgModule({
  declarations: [PointDemoComponent],
  imports: [CommonModule, WmPointModule],
  exports: [PointDemoComponent]
})
export class PointDemoModule {}
