import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, AbstractControl } from "@angular/forms";

@Component({
  selector: "app-slider-demo",
  templateUrl: "./slider-demo.component.html",
  styleUrls: ["./slider-demo.component.scss"]
})
export class SliderDemoComponent implements OnInit {
  sliderValue = 60;
  max = 200;

  templateHtml = `
    <form>
      <div [style.width.px]="500">
        <rbc-slider [(ngModel)]="sliderValue" name="select" [max]="max" display="true"></rbc-slider>
      </div>
    </form>
  `;
  reactiveHtml = `
    <form [formGroup]="form">
      <div [style.width.px]="500">
        <rbc-slider formControlName="slider" [max]="max" display="true"></rbc-slider>
      </div>
    </form>
  `;

  form: FormGroup;
  constructor(fb: FormBuilder) {
    this.form = fb.group({
      slider: [40]
    });
  }

  get slider(): AbstractControl {
    return this.form.get("slider");
  }

  ngOnInit() {}
}
