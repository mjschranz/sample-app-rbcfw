import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SliderDemoComponent } from "./slider-demo.component";
import { WmSliderModule } from "rbc-wm-framework-angular";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [SliderDemoComponent],
  imports: [CommonModule, WmSliderModule, FormsModule, ReactiveFormsModule],
  exports: [SliderDemoComponent]
})
export class SliderDemoModule {}
