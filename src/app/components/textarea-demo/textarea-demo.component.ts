import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, AbstractControl } from "@angular/forms";

@Component({
  selector: "app-textarea-demo",
  templateUrl: "./textarea-demo.component.html",
  styleUrls: ["./textarea-demo.component.scss"]
})
export class TextareaDemoComponent implements OnInit {
  textareaValue = "Template Init";
  form: FormGroup;

  templateHtml = `
    <form>
      <rbc-textarea [(ngModel)]="textareaValue" name="textarea"></rbc-textarea>
    </form>
  `;

  reactiveHtml = `
    <form [formGroup]="form">
      <rbc-textarea formControlName="textarea"></rbc-textarea>
    </form>
  `;

  constructor(fb: FormBuilder) {
    this.form = fb.group({
      textarea: ["Reactive Form Init"]
    });
  }

  get textarea(): AbstractControl {
    return this.form.get("textarea");
  }

  ngOnInit() {}
}
