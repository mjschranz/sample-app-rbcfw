import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TextareaDemoComponent } from "./textarea-demo.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { WmTextareaModule } from "rbc-wm-framework-angular";

@NgModule({
  declarations: [TextareaDemoComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, WmTextareaModule],
  exports: [TextareaDemoComponent]
})
export class TextareaDemoModule {}
