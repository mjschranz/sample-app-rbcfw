import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ProgressbarDemoComponent } from "./progressbar-demo.component";
import { WmProgressbarModule } from "rbc-wm-framework-angular";

@NgModule({
  declarations: [ProgressbarDemoComponent],
  imports: [CommonModule, WmProgressbarModule],
  exports: [ProgressbarDemoComponent]
})
export class ProgressbarDemoModule {}
