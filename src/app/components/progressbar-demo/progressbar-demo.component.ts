import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-progressbar-demo",
  templateUrl: "./progressbar-demo.component.html",
  styleUrls: ["./progressbar-demo.component.scss"]
})
export class ProgressbarDemoComponent implements OnInit {
  progressbarContent = `
    <div class="d-flex jc-space-around">
      <div [style.width.px]="100">
        <rbc-progressbar progress="40%"></rbc-progressbar>
      </div>
      <div [style.width.px]="100">
        <rbc-progressbar round="true" [progress]="progressVal"></rbc-progressbar>
      </div>
      <div [style.width.px]="100">
        <rbc-progressbar size="medium" progress="40%"></rbc-progressbar>
      </div>
      <div [style.width.px]="100">
        <rbc-progressbar size="large" progress="40%"></rbc-progressbar>
      </div>
    </div>
  `;

  progressVal = "20%";

  constructor() {
    setInterval(() => {
      const currProgress = parseInt(this.progressVal, 10);

      if (currProgress === 100) {
        this.progressVal = "20%";
      } else {
        this.progressVal = `${currProgress + 10}%`;
      }
    }, 1500);
  }

  ngOnInit() {}
}
