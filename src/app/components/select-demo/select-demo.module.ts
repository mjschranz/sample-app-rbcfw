import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SelectDemoComponent } from "./select-demo.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { WmSelectModule } from "rbc-wm-framework-angular";

@NgModule({
  declarations: [SelectDemoComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, WmSelectModule],
  exports: [SelectDemoComponent]
})
export class SelectDemoModule {}
