import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, AbstractControl } from "@angular/forms";

@Component({
  selector: "app-select-demo",
  templateUrl: "./select-demo.component.html",
  styleUrls: ["./select-demo.component.scss"]
})
export class SelectDemoComponent implements OnInit {
  selectOptions = [
    {
      label: "English",
      value: "en",
      id: Math.round(Math.random() * 1000000)
    },
    {
      label: "French",
      value: "fr",
      id: Math.round(Math.random() * 1000000)
    }
  ];

  selectOptionsReactive = [
    {
      label: "English",
      value: "en",
      id: Math.round(Math.random() * 1000000)
    },
    {
      label: "French",
      value: "fr",
      id: Math.round(Math.random() * 1000000)
    }
  ];
  form: FormGroup;

  selectValue = "en";

  templateHtml = `
    <form>
      <rbc-select [(ngModel)]="selectValue" [options]="selectOptions" name="select" label="Select Demo"></rbc-select>
    </form>
  `;

  reactiveHtml = `
    <form [formGroup]="form">
      <rbc-select formControlName="select" [options]="selectOptions" name="select" label="Select Demo"></rbc-select>
    </form>
  `;
  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      select: [null]
    });
  }

  get select(): AbstractControl {
    return this.form.get("select");
  }

  ngOnInit() {}
}
