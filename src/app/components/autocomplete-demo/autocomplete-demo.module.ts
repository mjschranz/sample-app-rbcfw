import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AutocompleteDemoComponent } from "./autocomplete-demo.component";
import { WmAutocompleteModule } from "rbc-wm-framework-angular";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [AutocompleteDemoComponent],
  imports: [CommonModule, WmAutocompleteModule, FormsModule, ReactiveFormsModule],
  exports: [AutocompleteDemoComponent]
})
export class AutocompleteDemoModule {}
