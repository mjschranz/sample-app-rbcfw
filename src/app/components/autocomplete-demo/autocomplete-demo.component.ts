import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, AbstractControl } from "@angular/forms";

@Component({
  selector: "app-autocomplete-demo",
  templateUrl: "./autocomplete-demo.component.html",
  styleUrls: ["./autocomplete-demo.component.scss"]
})
export class AutocompleteDemoComponent implements OnInit {
  autoCompleteOptions = [
    {
      key: "RBF665",
      value: "665"
    },
    {
      key: "RBF532",
      value: "532"
    },
    {
      key: "RBF843",
      value: "843"
    },
    {
      key: "RBF123",
      value: "123"
    },
    {
      key: "RBF663",
      value: "663"
    }
  ];

  preselectValue = {
    key: "RBF843",
    value: "843"
  };

  form: FormGroup;

  templateHtml = `
    <form>
      <div [style.width.px]="300">
        <rbc-autocomplete
          [options]="autoCompleteOptions"
          [(ngModel)]="preselectValue"
          name="autocomplete"
          label="Autocomplete Demo"
          placeholder="Enter a query"
        ></rbc-autocomplete>
      </div>
    </form>
  `;

  reactiveHtml = `
    <form class="mt-1" [formGroup]="form">
      <div [style.width.px]="300">
        <rbc-autocomplete
          [options]="autoCompleteOptions"
          formControlName="autoComplete"
          name="autocomplete"
          label="Autocomplete Demo"
          placeholder="Enter a query"
        ></rbc-autocomplete>
      </div>
    </form>
  `;

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      autoComplete: []
    });
  }

  get autoComplete(): AbstractControl {
    return this.form.get("autoComplete");
  }

  ngOnInit() {}
}
