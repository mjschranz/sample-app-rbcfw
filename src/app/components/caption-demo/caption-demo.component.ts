import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-caption-demo",
  templateUrl: "./caption-demo.component.html",
  styleUrls: ["./caption-demo.component.scss"]
})
export class CaptionDemoComponent implements OnInit {
  captionContent = `
    <div class="mt-1 d-flex jc-space-around">
      <rbc-caption text="Caption"></rbc-caption>

      <rbc-caption text="Caption" smallText="With small text"></rbc-caption>

      <rbc-caption text="Caption" smallText="With regular text" smallTextSize="regular"></rbc-caption>

      <rbc-caption avatarTextColor="warm-red" icon="heart"></rbc-caption>

      <rbc-caption avatarText="Rb" avatarColor="teal"></rbc-caption>
    </div>
  `;
  constructor() {}

  ngOnInit() {}
}
