import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CaptionDemoComponent } from "./caption-demo.component";
import { WmCaptionModule } from "rbc-wm-framework-angular";

@NgModule({
  declarations: [CaptionDemoComponent],
  imports: [CommonModule, WmCaptionModule],
  exports: [CaptionDemoComponent]
})
export class CaptionDemoModule {}
