import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { CaptionDemoComponent } from "./caption-demo.component";

describe("CaptionDemoComponent", () => {
  let component: CaptionDemoComponent;
  let fixture: ComponentFixture<CaptionDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CaptionDemoComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaptionDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
