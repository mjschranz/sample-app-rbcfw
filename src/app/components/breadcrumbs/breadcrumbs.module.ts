import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BreadcrumbsDemoComponent } from "./breadcrumbs-demo.component";
import { WmBreadcrumbsModule } from "rbc-wm-framework-angular";

@NgModule({
  declarations: [BreadcrumbsDemoComponent],
  imports: [CommonModule, WmBreadcrumbsModule],
  exports: [BreadcrumbsDemoComponent]
})
export class BreadcrumbsModule {}
