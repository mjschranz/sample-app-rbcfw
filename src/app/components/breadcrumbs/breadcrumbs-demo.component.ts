import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-breadcrumbs-demo",
  templateUrl: "./breadcrumbs-demo.component.html",
  styleUrls: ["./breadcrumbs-demo.component.scss"]
})
export class BreadcrumbsDemoComponent implements OnInit {
  breadcrumbOptions = [
    {
      text: "Home",
      link: "/"
    },
    {
      text: "Google",
      href: "https://google.ca"
    },
    {
      text: "Other"
    }
  ];

  breadcrumbOptionsString = `
    [
      {
        text: "Home",
        link: "/"
      },
      {
        text: "Google",
        href: "https://google.ca"
      },
      {
        text: "Other"
      }
    ]
  `;

  constructor() {}

  ngOnInit() {}
}
