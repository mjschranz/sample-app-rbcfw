import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { BreadcrumbsDemoComponent } from "./breadcrumbs-demo.component";

describe("BreadcrumbsDemoComponent", () => {
  let component: BreadcrumbsDemoComponent;
  let fixture: ComponentFixture<BreadcrumbsDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BreadcrumbsDemoComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreadcrumbsDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
