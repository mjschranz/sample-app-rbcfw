import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, AbstractControl } from "@angular/forms";

@Component({
  selector: "app-datepicker-demo",
  templateUrl: "./datepicker-demo.component.html",
  styleUrls: ["./datepicker-demo.component.scss"]
})
export class DatepickerDemoComponent implements OnInit {
  datepickerValue = new Date();
  form: FormGroup;

  templateHtml = `
    <form>
      <rbc-datepicker
        [(ngModel)]="datepickerValue"
        errorLabel="Invalid Date"
        label="Select Date"
        name="datePicker"
      ></rbc-datepicker>
    </form>
  `;
  reactiveHtml = `
    <form [formGroup]="form">
      <rbc-datepicker errorLabel="Invalid Date" label="Select Date" formControlName="datePicker"></rbc-datepicker>
    </form>
  `;
  constructor(fb: FormBuilder) {
    this.form = fb.group({
      datePicker: []
    });
  }

  get datePicker(): AbstractControl {
    return this.form.get("datePicker");
  }

  ngOnInit() {}
}
