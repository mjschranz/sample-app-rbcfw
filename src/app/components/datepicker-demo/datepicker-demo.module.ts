import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DatepickerDemoComponent } from "./datepicker-demo.component";
import { WmDatepickerModule } from "rbc-wm-framework-angular";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [DatepickerDemoComponent],
  imports: [CommonModule, WmDatepickerModule, FormsModule, ReactiveFormsModule],
  exports: [DatepickerDemoComponent]
})
export class DatepickerDemoModule {}
