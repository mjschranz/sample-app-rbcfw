import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CheckboxDemoComponent } from "./checkbox-demo.component";
import { WmCheckboxModule } from "rbc-wm-framework-angular";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [CheckboxDemoComponent],
  imports: [CommonModule, WmCheckboxModule, FormsModule, ReactiveFormsModule],
  exports: [CheckboxDemoComponent]
})
export class CheckboxDemoModule {}
