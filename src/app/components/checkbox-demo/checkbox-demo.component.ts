import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, AbstractControl } from "@angular/forms";

@Component({
  selector: "app-checkbox-demo",
  templateUrl: "./checkbox-demo.component.html",
  styleUrls: ["./checkbox-demo.component.scss"]
})
export class CheckboxDemoComponent implements OnInit {
  checkedState = false;
  form: FormGroup;

  templateHtml = `
    <form>
      <rbc-checkbox [(ngModel)]="checkedState" name="checkbox">Yes</rbc-checkbox>
    </form>
  `;

  reactiveHtml = `
    <form [formGroup]="form">
      <rbc-checkbox formControlName="checkbox">Yes</rbc-checkbox>
    </form>
  `;

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      checkbox: [this.checkedState]
    });
  }

  get checkbox(): AbstractControl {
    return this.form.get("checkbox");
  }

  ngOnInit() {}
}
