import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, AbstractControl } from "@angular/forms";

@Component({
  selector: "app-input-demo",
  templateUrl: "./input-demo.component.html",
  styleUrls: ["./input-demo.component.scss"]
})
export class InputDemoComponent implements OnInit {
  inputValue = "I am using the WM Input Component";

  form: FormGroup;
  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      input: [this.inputValue]
    });
  }

  templateHtml = `
    <form>
      <div [style.width.px]="300">
        <rbc-input [(ngModel)]="inputValue" name="inputTest" helpText="I am helper text" label="I am a label"></rbc-input>
      </div>
    </form>
  `;

  reactiveHtml = `
    <form [formGroup]="form">
      <div [style.width.px]="300">
        <rbc-input formControlName="input" name="inputTest" helpText="I am helper text" label="I am a label"></rbc-input>
      </div>
    </form>
  `;

  get input(): AbstractControl {
    return this.form.get("input");
  }

  ngOnInit() {}
}
