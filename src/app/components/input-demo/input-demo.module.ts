import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { InputDemoComponent } from "./input-demo.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { WmInputModule } from "rbc-wm-framework-angular";

@NgModule({
  declarations: [InputDemoComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, WmInputModule],
  exports: [InputDemoComponent]
})
export class InputDemoModule {}
