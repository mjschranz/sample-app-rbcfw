import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-tab-demo",
  templateUrl: "./tab-demo.component.html",
  styleUrls: ["./tab-demo.component.scss"]
})
export class TabDemoComponent implements OnInit {
  tabContent = `
    <rbc-tab>
      <rbc-tab-content name="First">
        First
      </rbc-tab-content>
      <rbc-tab-content name="Second">
        Second
      </rbc-tab-content>
      <rbc-tab-content name="Third">
        Third
      </rbc-tab-content>
    </rbc-tab>
  `;
  constructor() {}

  ngOnInit() {}
}
