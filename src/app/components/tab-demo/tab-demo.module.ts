import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TabDemoComponent } from "./tab-demo.component";
import { WmTabModule } from "rbc-wm-framework-angular";

@NgModule({
  declarations: [TabDemoComponent],
  imports: [CommonModule, WmTabModule],
  exports: [TabDemoComponent]
})
export class TabDemoModule {}
