import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { RadioComponent } from "./radio.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { WmRadioModule } from "rbc-wm-framework-angular";

const components = [RadioComponent];

@NgModule({
  declarations: components,
  imports: [CommonModule, FormsModule, ReactiveFormsModule, WmRadioModule],
  exports: components
})
export class RadioModule {}
