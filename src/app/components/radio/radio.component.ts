import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, AbstractControl } from "@angular/forms";

@Component({
  selector: "app-radio",
  templateUrl: "./radio.component.html",
  styleUrls: ["./radio.component.scss"]
})
export class RadioComponent implements OnInit {
  form: FormGroup;
  templateRadioValue = "drama";
  reactiveRadioValue = "action";

  reactiveHtml = `
    <form [formGroup]="form">
      <rbc-radio formControlName="movie" value="action">Action</rbc-radio>
      <rbc-radio formControlName="movie" value="drama" [style.marginLeft.px]="10">Drama</rbc-radio>
      <rbc-radio formControlName="movie" value="comedy" [style.marginLeft.px]="10">Comedy</rbc-radio>
    </form>
  `;

  templateHtml = `
    <form>
      <rbc-radio name="movie" value="action" [(ngModel)]="templateRadioValue">Action</rbc-radio>
      <rbc-radio name="movie" value="drama" [(ngModel)]="templateRadioValue" [style.marginLeft.px]="10">Drama</rbc-radio>
      <rbc-radio name="movie" value="comedy" [(ngModel)]="templateRadioValue" [style.marginLeft.px]="10" disabled="true"
        >Comedy</rbc-radio
      >
    </form>
  `;

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      movie: [this.reactiveRadioValue]
    });
  }

  get movie(): AbstractControl {
    return this.form.get("movie");
  }

  ngOnInit() {
    this.movie.valueChanges.subscribe(val => (this.reactiveRadioValue = val));
  }
}
