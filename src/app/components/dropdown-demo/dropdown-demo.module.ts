import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DropdownDemoComponent } from "./dropdown-demo.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { WmDropdownModule } from "rbc-wm-framework-angular";

@NgModule({
  declarations: [DropdownDemoComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, WmDropdownModule],
  exports: [DropdownDemoComponent]
})
export class DropdownDemoModule {}
