import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, AbstractControl } from "@angular/forms";

@Component({
  selector: "app-dropdown-demo",
  templateUrl: "./dropdown-demo.component.html",
  styleUrls: ["./dropdown-demo.component.scss"]
})
export class DropdownDemoComponent implements OnInit {
  form: FormGroup;

  templateHtml = `
    <form>
      <rbc-dropdown
        [(ngModel)]="dropdownValue"
        [options]="dropdownOptions"
        name="dropdown"
        [height]="dropdownHeight"
        label="Dropdown Demo"
      ></rbc-dropdown>
    </form>
  `;
  reactiveHtml = `
    <form [formGroup]="form">
      <rbc-dropdown
        formControlName="dropdown"
        [options]="dropdownReactiveOptions"
        label="Dropdown Demo"
        [height]="dropdownHeight"
      ></rbc-dropdown>
    </form>
  `;
  dropdownValue = "en";
  dropdownHeight = 200;
  dropdownOptions = [
    {
      label: "English",
      value: "en",
      id: Math.round(Math.random() * 1000000)
    },
    {
      label: "French",
      value: "fr",
      id: Math.round(Math.random() * 1000000)
    }
  ];
  dropdownReactiveOptions = [
    {
      label: "English",
      value: "en",
      id: Math.round(Math.random() * 1000000)
    },
    {
      label: "French",
      value: "fr",
      id: Math.round(Math.random() * 1000000)
    },
    { label: "Spanish", value: "es", id: Math.round(Math.random() * 1000000) }
  ];

  constructor(fb: FormBuilder) {
    this.form = fb.group({
      dropdown: ["es"]
    });
  }

  get dropdown(): AbstractControl {
    return this.form.get("dropdown");
  }

  ngOnInit() {}
}
