import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ExpandableDemoComponent } from "./expandable-demo.component";
import { WmExpandableModule } from "rbc-wm-framework-angular";

@NgModule({
  declarations: [ExpandableDemoComponent],
  imports: [CommonModule, WmExpandableModule],
  exports: [ExpandableDemoComponent]
})
export class ExpandableDemoModule {}
