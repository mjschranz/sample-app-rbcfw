import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-expandable-demo",
  templateUrl: "./expandable-demo.component.html",
  styleUrls: ["./expandable-demo.component.scss"]
})
export class ExpandableDemoComponent implements OnInit {
  expandedContent = `
    <rbc-expandable arrow="true" expanded="true">
      <div class="trigger">Expanded content</div>
      <div>
        I am expanded content.
      </div>
    </rbc-expandable>
  `;
  constructor() {}

  ngOnInit() {}
}
