import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ExpandableDemoComponent } from "./expandable-demo.component";

describe("ExpandableDemoComponent", () => {
  let component: ExpandableDemoComponent;
  let fixture: ComponentFixture<ExpandableDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExpandableDemoComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpandableDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
