import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RadioComponent } from "./radio/radio.component";
import { BreadcrumbsDemoComponent } from "./breadcrumbs/breadcrumbs-demo.component";
import { ExpandableDemoComponent } from "./expandable-demo/expandable-demo.component";
import { AlertDemoComponent } from "./alert-demo/alert-demo.component";
import { ButtonDemoComponent } from "./button-demo/button-demo.component";
import { CaptionDemoComponent } from "./caption-demo/caption-demo.component";
import { MenuDemoComponent } from "./menu-demo/menu-demo.component";
import { PointDemoComponent } from "./point-demo/point-demo.component";
import { ProgressbarDemoComponent } from "./progressbar-demo/progressbar-demo.component";
import { TooltipDemoComponent } from "./tooltip-demo/tooltip-demo.component";
import { TagDemoComponent } from "./tag-demo/tag-demo.component";
import { TabDemoComponent } from "./tab-demo/tab-demo.component";
import { AutocompleteDemoComponent } from "./autocomplete-demo/autocomplete-demo.component";
import { CheckboxDemoComponent } from "./checkbox-demo/checkbox-demo.component";
import { InputDemoComponent } from "./input-demo/input-demo.component";
import { SelectDemoComponent } from "./select-demo/select-demo.component";
import { MultiselectDemoComponent } from "./multiselect-demo/multiselect-demo.component";
import { SwitchDemoComponent } from "./switch-demo/switch-demo.component";
import { TextareaDemoComponent } from "./textarea-demo/textarea-demo.component";
import { DatatableDemoComponent } from "./datatable-demo/datatable-demo.component";
import { ButtontoggleDemoComponent } from "./buttontoggle-demo/buttontoggle-demo.component";
import { SliderDemoComponent } from "./slider-demo/slider-demo.component";
import { DropdownDemoComponent } from "./dropdown-demo/dropdown-demo.component";
import { CardDemoComponent } from "./card-demo/card-demo.component";
import { DatepickerDemoComponent } from "./datepicker-demo/datepicker-demo.component";

const routes: Routes = [
  {
    path: "components",
    children: [
      {
        path: "alert",
        component: AlertDemoComponent
      },
      {
        path: "autocomplete",
        component: AutocompleteDemoComponent
      },
      {
        path: "button",
        component: ButtonDemoComponent
      },
      {
        path: "button-toggle",
        component: ButtontoggleDemoComponent
      },
      {
        path: "breadcrumbs",
        component: BreadcrumbsDemoComponent
      },
      {
        path: "caption",
        component: CaptionDemoComponent
      },
      {
        path: "card",
        component: CardDemoComponent
      },
      {
        path: "checkbox",
        component: CheckboxDemoComponent
      },
      {
        path: "data-table",
        component: DatatableDemoComponent
      },
      {
        path: "datepicker",
        component: DatepickerDemoComponent
      },
      {
        path: "dropdown",
        component: DropdownDemoComponent
      },
      {
        path: "expandable",
        component: ExpandableDemoComponent
      },
      {
        path: "input",
        component: InputDemoComponent
      },
      {
        path: "menu",
        component: MenuDemoComponent
      },
      {
        path: "point",
        component: PointDemoComponent
      },
      {
        path: "progressbar",
        component: ProgressbarDemoComponent
      },
      {
        path: "radio",
        component: RadioComponent
      },
      {
        path: "select",
        component: SelectDemoComponent
      },
      {
        path: "multi-select",
        component: MultiselectDemoComponent
      },
      {
        path: "slider",
        component: SliderDemoComponent
      },
      {
        path: "switch",
        component: SwitchDemoComponent
      },
      {
        path: "tab",
        component: TabDemoComponent
      },
      {
        path: "tag",
        component: TagDemoComponent
      },
      {
        path: "textarea",
        component: TextareaDemoComponent
      },
      {
        path: "tooltip",
        component: TooltipDemoComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentsRoutingModule {}
