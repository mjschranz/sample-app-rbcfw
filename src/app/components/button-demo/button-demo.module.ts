import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ButtonDemoComponent } from "./button-demo.component";
import { WmButtonModule } from "rbc-wm-framework-angular";

@NgModule({
  declarations: [ButtonDemoComponent],
  imports: [CommonModule, WmButtonModule],
  exports: [ButtonDemoComponent]
})
export class ButtonDemoModule {}
