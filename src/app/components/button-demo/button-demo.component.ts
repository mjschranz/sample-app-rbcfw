import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-button-demo",
  templateUrl: "./button-demo.component.html",
  styleUrls: ["./button-demo.component.scss"]
})
export class ButtonDemoComponent implements OnInit {
  loadingFlag = false;

  buttonContent = `
    <div class="mt-1">
      <rbc-button [loading]="loadingFlag">I am a button!</rbc-button>
    </div>
  `;
  constructor() {
    setInterval(() => {
      this.loadingFlag = !this.loadingFlag;
    }, 3000);
  }

  ngOnInit() {}
}
