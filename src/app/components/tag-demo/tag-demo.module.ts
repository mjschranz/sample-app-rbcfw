import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TagDemoComponent } from "./tag-demo.component";
import { WmTagModule } from "rbc-wm-framework-angular";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [TagDemoComponent],
  imports: [CommonModule, WmTagModule, FormsModule, ReactiveFormsModule],
  exports: [TagDemoComponent]
})
export class TagDemoModule {}
