import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, AbstractControl } from "@angular/forms";

@Component({
  selector: "app-tag-demo",
  templateUrl: "./tag-demo.component.html",
  styleUrls: ["./tag-demo.component.scss"]
})
export class TagDemoComponent implements OnInit {
  gamesValue = true;
  moviesValue = false;
  form: FormGroup;

  templateHtml = `
    <form>
      <div class="mt-1 d-flex jc-space-around">
        <rbc-tag [(ngModel)]="gamesValue" name="games" checkable="true">Games</rbc-tag>
        <rbc-tag [(ngModel)]="moviesValue" name="movies" checkable="true">Movies</rbc-tag>
      </div>
    </form>
  `;

  reactiveHtml = `
    <form [formGroup]="form">
      <div class="mt-1 d-flex jc-space-around">
        <rbc-tag formControlName="games" checkable="true">Games</rbc-tag>
        <rbc-tag formControlName="movies" checkable="true">Movies</rbc-tag>
      </div>
    </form>
  `;

  constructor(fb: FormBuilder) {
    this.form = fb.group({
      games: [false],
      movies: [true]
    });
  }

  get games(): AbstractControl {
    return this.form.get("games");
  }

  get movies(): AbstractControl {
    return this.form.get("movies");
  }

  ngOnInit() {}
}
