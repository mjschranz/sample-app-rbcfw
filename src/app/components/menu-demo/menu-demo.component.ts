import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-menu-demo",
  templateUrl: "./menu-demo.component.html",
  styleUrls: ["./menu-demo.component.scss"]
})
export class MenuDemoComponent implements OnInit {
  menuContent = `
    <div class="mt-1 d-flex jc-space-around">
      <rbc-menu>
        <div rbc-menu-item>Copy</div>
        <div rbc-menu-item>Cut</div>
        <div rbc-menu-item>Paste</div>
      </rbc-menu>
    </div>

  `;
  constructor() {}

  ngOnInit() {}
}
