import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MenuDemoComponent } from "./menu-demo.component";
import { WmMenuModule } from "rbc-wm-framework-angular";

@NgModule({
  declarations: [MenuDemoComponent],
  imports: [CommonModule, WmMenuModule],
  exports: [MenuDemoComponent]
})
export class MenuDemoModule {}
