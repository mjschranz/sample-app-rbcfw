import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";

@Component({
  selector: "app-tooltip-demo",
  templateUrl: "./tooltip-demo.component.html",
  styleUrls: ["./tooltip-demo.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TooltipDemoComponent implements OnInit {
  tooltipContent = `
    <div class="d-flex jc-space-around">
      <rbc-tooltip icon="calendar" message="Calendar"></rbc-tooltip>
      <rbc-tooltip message="*Minimum initial investment is $25,000" title="Initial Investment"></rbc-tooltip>
      <rbc-tooltip title="Highlights" icon="star">
        <ul>
          <li>
            Lorem ipsum dolor sit amet, consectetur adipiscing Phasellus eget venenatis risus, ut vulputate augue. Mauris
            mollis pretium quam, a finibus lacus.
          </li>
          <li>
            Lorem ipsum dolor sit amet, consectetur adipiscing Phasellus eget venenatis risus, ut vulputate augue. Mauris
            mollis pretium quam, a finibus lacus.
          </li>
        </ul>
      </rbc-tooltip>
    </div>
  `;
  constructor() {}

  ngOnInit() {}
}
