import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TooltipDemoComponent } from "./tooltip-demo.component";
import { WmTooltipModule } from "rbc-wm-framework-angular";

@NgModule({
  declarations: [TooltipDemoComponent],
  imports: [CommonModule, WmTooltipModule],
  exports: [TooltipDemoComponent]
})
export class TooltipDemoModule {}
