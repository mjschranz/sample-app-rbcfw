import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-alert-demo",
  templateUrl: "./alert-demo.component.html",
  styleUrls: ["./alert-demo.component.scss"]
})
export class AlertDemoComponent implements OnInit {
  alertContent = `
    <rbc-alert header="WM Component Library">I am the alert component from the WM component library</rbc-alert>
    <rbc-alert type="danger" header="WM Component Library">I am the alert component from the WM component library</rbc-alert>
    <rbc-alert type="warning" header="WM Component Library">I am the alert component from the WM component library</rbc-alert>
    <rbc-alert type="success" header="WM Component Library">I am the alert component from the WM component library</rbc-alert>
  `;
  constructor() {}

  ngOnInit() {}
}
