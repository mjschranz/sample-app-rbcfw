import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AlertDemoComponent } from "./alert-demo.component";
import { WmAlertModule } from "rbc-wm-framework-angular";

@NgModule({
  declarations: [AlertDemoComponent],
  imports: [CommonModule, WmAlertModule],
  exports: [AlertDemoComponent]
})
export class AlertDemoModule {}
