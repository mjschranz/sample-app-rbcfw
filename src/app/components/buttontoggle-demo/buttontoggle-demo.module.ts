import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ButtontoggleDemoComponent } from "./buttontoggle-demo.component";
import { WmButtonToggleContainerModule, WmButtonToggleModule } from "rbc-wm-framework-angular";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [ButtontoggleDemoComponent],
  imports: [CommonModule, WmButtonToggleContainerModule, WmButtonToggleModule, FormsModule, ReactiveFormsModule],
  exports: [ButtontoggleDemoComponent]
})
export class ButtontoggleDemoModule {}
