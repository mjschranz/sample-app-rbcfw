import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, AbstractControl } from "@angular/forms";

@Component({
  selector: "app-buttontoggle-demo",
  templateUrl: "./buttontoggle-demo.component.html",
  styleUrls: ["./buttontoggle-demo.component.scss"]
})
export class ButtontoggleDemoComponent implements OnInit {
  templateModel = "right";

  templateHtml = `
    <form>
      <rbc-toggle-container color="dark">
        <rbc-toggle name="position" value="left" [(ngModel)]="templateModel">
          Left
        </rbc-toggle>
        <rbc-toggle name="position" value="middle" [(ngModel)]="templateModel">
          Middle
        </rbc-toggle>
        <rbc-toggle name="position" value="right" [(ngModel)]="templateModel">
          Right
        </rbc-toggle>
      </rbc-toggle-container>
    </form>
  `;
  reactiveHtml = `
    <form [formGroup]="form">
      <rbc-toggle-container color="dark">
        <rbc-toggle formControlName="position" value="left">
          Left
        </rbc-toggle>
        <rbc-toggle formControlName="position" value="middle">
          Middle
        </rbc-toggle>
        <rbc-toggle formControlName="position" value="right">
          Right
        </rbc-toggle>
      </rbc-toggle-container>
    </form>
  `;

  form: FormGroup;
  constructor(fb: FormBuilder) {
    this.form = fb.group({
      position: ["left"]
    });
  }

  get position(): AbstractControl {
    return this.form.get("position");
  }

  ngOnInit() {}
}
