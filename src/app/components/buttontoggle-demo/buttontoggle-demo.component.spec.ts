import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ButtontoggleDemoComponent } from "./buttontoggle-demo.component";

describe("ButtontoggleDemoComponent", () => {
  let component: ButtontoggleDemoComponent;
  let fixture: ComponentFixture<ButtontoggleDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ButtontoggleDemoComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtontoggleDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
