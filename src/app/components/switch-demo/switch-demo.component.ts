import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, AbstractControl } from "@angular/forms";

@Component({
  selector: "app-switch-demo",
  templateUrl: "./switch-demo.component.html",
  styleUrls: ["./switch-demo.component.scss"]
})
export class SwitchDemoComponent implements OnInit {
  switchValue = true;

  form: FormGroup;
  templateHtml = `
    <form>
      <rbc-switch [(ngModel)]="switchValue" name="switch" label="Permission Given"></rbc-switch>
    </form>
  `;
  reactiveHtml = `
    <form [formGroup]="form">
      <rbc-switch formControlName="switch" label="Permission Given"></rbc-switch>
    </form>
  `;

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      switch: [false]
    });
  }

  get switch(): AbstractControl {
    return this.form.get("switch");
  }

  ngOnInit() {}
}
