import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SwitchDemoComponent } from "./switch-demo.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { WmSwitchModule } from "rbc-wm-framework-angular";

@NgModule({
  declarations: [SwitchDemoComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, WmSwitchModule],
  exports: [SwitchDemoComponent]
})
export class SwitchDemoModule {}
