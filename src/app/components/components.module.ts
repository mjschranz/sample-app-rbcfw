import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ComponentsRoutingModule } from "./components-routing.module";
import { RadioModule } from "./radio/radio.module";
import { BreadcrumbsModule } from "./breadcrumbs/breadcrumbs.module";
import { ExpandableDemoModule } from "./expandable-demo/expandable-demo.module";
import { AlertDemoModule } from "./alert-demo/alert-demo.module";
import { ButtonDemoModule } from "./button-demo/button-demo.module";
import { CaptionDemoModule } from "./caption-demo/caption-demo.module";
import { MenuDemoModule } from "./menu-demo/menu-demo.module";
import { PointDemoModule } from "./point-demo/point-demo.module";
import { ProgressbarDemoModule } from "./progressbar-demo/progressbar-demo.module";
import { TagDemoModule } from "./tag-demo/tag-demo.module";
import { TooltipDemoModule } from "./tooltip-demo/tooltip-demo.module";
import { TabDemoModule } from "./tab-demo/tab-demo.module";
import { AutocompleteDemoModule } from "./autocomplete-demo/autocomplete-demo.module";
import { CheckboxDemoModule } from "./checkbox-demo/checkbox-demo.module";
import { DropdownDemoModule } from "./dropdown-demo/dropdown-demo.module";
import { InputDemoModule } from "./input-demo/input-demo.module";
import { MultiselectDemoModule } from "./multiselect-demo/multiselect-demo.module";
import { SelectDemoModule } from "./select-demo/select-demo.module";
import { SwitchDemoModule } from "./switch-demo/switch-demo.module";
import { TextareaDemoModule } from "./textarea-demo/textarea-demo.module";
import { DatatableDemoModule } from "./datatable-demo/datatable-demo.module";
import { ButtontoggleDemoModule } from "./buttontoggle-demo/buttontoggle-demo.module";
import { SliderDemoModule } from "./slider-demo/slider-demo.module";
import { CardDemoModule } from "./card-demo/card-demo.module";
import { DatepickerDemoModule } from "./datepicker-demo/datepicker-demo.module";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ComponentsRoutingModule,
    RadioModule,
    BreadcrumbsModule,
    ExpandableDemoModule,
    AlertDemoModule,
    ButtonDemoModule,
    CaptionDemoModule,
    MenuDemoModule,
    PointDemoModule,
    ProgressbarDemoModule,
    TagDemoModule,
    TooltipDemoModule,
    TabDemoModule,
    AutocompleteDemoModule,
    CheckboxDemoModule,
    DropdownDemoModule,
    InputDemoModule,
    MultiselectDemoModule,
    SelectDemoModule,
    SwitchDemoModule,
    TextareaDemoModule,
    DatatableDemoModule,
    ButtontoggleDemoModule,
    SliderDemoModule,
    DropdownDemoModule,
    CardDemoModule,
    DatepickerDemoModule
  ]
})
export class ComponentsModule {}
