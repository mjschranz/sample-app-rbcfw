import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DatatableDemoComponent } from "./datatable-demo.component";
import { WmDatatableModule, WmTabModule } from "rbc-wm-framework-angular";

@NgModule({
  declarations: [DatatableDemoComponent],
  imports: [CommonModule, WmDatatableModule, WmTabModule],
  exports: [DatatableDemoComponent]
})
export class DatatableDemoModule {}
