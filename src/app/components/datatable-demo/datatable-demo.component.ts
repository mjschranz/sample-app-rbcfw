import { Component, OnInit } from "@angular/core";
import { DatatableTestData } from "rbc-wm-framework-angular";

@Component({
  selector: "app-datatable-demo",
  templateUrl: "./datatable-demo.component.html",
  styleUrls: ["./datatable-demo.component.scss"]
})
export class DatatableDemoComponent implements OnInit {
  dataTableTesting;

  constructor() {
    this.dataTableTesting = JSON.parse(JSON.stringify(DatatableTestData));
  }

  ngOnInit() {}
}
