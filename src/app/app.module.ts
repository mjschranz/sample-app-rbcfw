import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ComponentsModule } from "./components/components.module";
import { WmTopnavModule, WmSidenavModule, WmBadgeModule } from "rbc-wm-framework-angular";

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, AppRoutingModule, ComponentsModule, WmTopnavModule, WmSidenavModule, WmBadgeModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
