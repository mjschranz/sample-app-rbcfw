import { Component } from "@angular/core";
import { SidenavConfig } from "rbc-wm-framework-angular";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  // topnav
  topnavid = "top-nav-id-sample";
  fullWidth = true;
  logoLink = "/";
  logoAlt = "logo-alt";
  logo = "/assets/rbc-wm-logo-en.svg";
  searchPlaceholder = "Search the site";
  dropdownId = "dropdown-id-sample";
  dropdown = true;
  dropdownArrow = false;
  dropdownPosition = "center";
  dropdownIcon = "globe";
  dropdownIconWeight = "light";
  withImageId = "item-with-image-id-sample";
  control = true;
  imageUrl = "https://i0.wp.com/zblogged.com/wp-content/uploads/2019/02/FakeDP.jpeg?resize=567%2C580&ssl=1";
  href = "/";
  target = "_blank";

  // Side-nav
  id = "side-nav";
  toggle = "icon";
  arrows = true;
  hamburgerPosition = "left";
  showHamburger = true;
  search = false;
  iconWeight = "light";
  isExpanded = false;

  testData: { navIsToggled: boolean; sideNavItems: SidenavConfig[] } = {
    navIsToggled: false,
    sideNavItems: [
      {
        name: "components",
        displayName: "Components",
        icon: "cog",
        children: [
          {
            name: "alert",
            displayName: "Alert",
            routerLinkOptions: {
              to: ["components", "alert"]
            }
          },
          {
            name: "autocomplete",
            displayName: "Autocomplete",
            routerLinkOptions: {
              to: ["components", "autocomplete"]
            }
          },
          {
            name: "breadcrumbs",
            displayName: "Breadcrumbs",
            routerLinkOptions: {
              to: ["components", "breadcrumbs"]
            }
          },
          {
            name: "button",
            displayName: "Button",
            routerLinkOptions: {
              to: ["components", "button"]
            }
          },
          {
            name: "button-toggle",
            displayName: "Button Toggle",
            routerLinkOptions: {
              to: ["components", "button-toggle"]
            }
          },
          {
            name: "caption",
            displayName: "Caption",
            routerLinkOptions: {
              to: ["components", "caption"]
            }
          },
          {
            name: "card",
            displayName: "Card",
            routerLinkOptions: {
              to: ["components", "card"]
            }
          },
          {
            name: "checkbox",
            displayName: "Checkbox",
            routerLinkOptions: {
              to: ["components", "checkbox"]
            }
          },
          {
            name: "data-table",
            displayName: "Data Table",
            routerLinkOptions: {
              to: ["components", "data-table"]
            }
          },
          {
            name: "datepicker",
            displayName: "Datepicker",
            routerLinkOptions: {
              to: ["components", "datepicker"]
            }
          },
          {
            name: "dropdown",
            displayName: "Dropdown",
            routerLinkOptions: {
              to: ["components", "dropdown"]
            }
          },
          {
            name: "expandable",
            displayName: "Expandable",
            routerLinkOptions: {
              to: ["components", "expandable"]
            }
          },
          {
            name: "input",
            displayName: "Input",
            routerLinkOptions: {
              to: ["components", "input"]
            }
          },
          {
            name: "menu",
            displayName: "Menu",
            routerLinkOptions: {
              to: ["components", "menu"]
            }
          },
          {
            name: "point",
            displayName: "Point",
            routerLinkOptions: {
              to: ["components", "point"]
            }
          },
          {
            name: "progressbar",
            displayName: "Progressbar",
            routerLinkOptions: {
              to: ["components", "progressbar"]
            }
          },
          {
            name: "radio",
            displayName: "Radio",
            routerLinkOptions: {
              to: ["components", "radio"]
            }
          },
          {
            name: "select",
            displayName: "Select",
            routerLinkOptions: {
              to: ["components", "select"]
            }
          },
          {
            name: "multi-select",
            displayName: "Select (Multi)",
            routerLinkOptions: {
              to: ["components", "multi-select"]
            }
          },
          {
            name: "slider",
            displayName: "Slider",
            routerLinkOptions: {
              to: ["components", "slider"]
            }
          },
          {
            name: "switch",
            displayName: "Switch",
            routerLinkOptions: {
              to: ["components", "switch"]
            }
          },
          {
            name: "tab",
            displayName: "Tab",
            routerLinkOptions: {
              to: ["components", "tab"]
            }
          },
          {
            name: "tag",
            displayName: "Tag",
            routerLinkOptions: {
              to: ["components", "tag"]
            }
          },
          {
            name: "textarea",
            displayName: "Textarea",
            routerLinkOptions: {
              to: ["components", "textarea"]
            }
          },
          {
            name: "tooltip",
            displayName: "Tooltip",
            routerLinkOptions: {
              to: ["components", "tooltip"]
            }
          }
        ]
      }
    ]
  };
}
